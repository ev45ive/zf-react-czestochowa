inc = ({type:'INC', payload:1})
dec = ({type:'DEC', payload:1});
addTodo = ({type:'ADD_TODO', payload:{id:123, name:'todo' }});

reducer = ( state, action ) => {
	switch(action.type){

        case 'INC': return {...state, counter:state.counter + action.payload }
        case 'DEC': return {...state, counter:state.counter - action.payload }
        case 'RESET': return {...state, counter:0 }
        case 'ADD_TODO': return {...state, todos: [...state.todos, action.payload] }

        default:
			return state
    }
}

[inc,inc,addTodo,dec,inc].reduce( reducer , {
 counter:0,
 todos:[]
})