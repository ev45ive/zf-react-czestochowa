import React, { ChangeEvent } from "react";
import { RouteComponentProps } from "react-router";

type P = {
  query: string | null;
  onSearch(query: string): void;
} & RouteComponentProps;

type S = {
  query: string;
};

export class SearchForm extends React.Component<P, S> {
  search = () => {
    this.props.history.replace({
      pathname: "/music",
      search: '?q='+this.state.query
    });

    this.props.onSearch(this.state.query);
  };

  handleChange = (event: ChangeEvent<HTMLInputElement>) =>
    this.setState({ query: event.target.value });

  constructor(props: P) {
    super(props);
    this.state = { query: props.query! };
  }

  // static getDerivedStateFromProps(props: P,state:S) {
  //   if (props.query) props.onSearch(props.query);

  //   return { query: state.query || props.query };
  // }

  render() {
    return (
      <div>
        <div className="input-group mb-3">
          <input
            className="form-control"
            placeholder="Search"
            value={this.state.query}
            onChange={this.handleChange}
            onKeyUp={e => e.key === "Enter" && this.search()}
          />

          <div className="input-group-append">
            <button className="btn btn-outline-secondary" onClick={this.search}>
              Search
            </button>
          </div>
        </div>
      </div>
    );
  }
}
