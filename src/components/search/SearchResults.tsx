import React from "react";
import { AlbumCard } from "./AlbumCard";
import "./SearchResults.css";
import { Album } from "../../models/Album";

type P = {
  results: Album[];
  error?: string
};

export const SearchResults: React.FC<P> = ({ results,error }) => {
  return (
    <div>
      {error}
      <div className="card-group">
        {results.map(result => (
          <AlbumCard album={result} key={result.id} />
        ))}
      </div>
    </div>
  );
};
