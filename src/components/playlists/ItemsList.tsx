import React from "react";
import { Playlist } from "../../models/Playlist";

type Props = {
  playlists: Playlist[];
  selected: Playlist | null;

  onSelect(selected: Playlist):void
};

export class ItemsList extends React.Component<Props> {

  select(playlist: Playlist) {
    this.props.onSelect(playlist)
  }
  
  // shouldComponentUpdate(nextProps:Props, nextState:{}) {
  //   return nextProps.playlists !== this.props.playlists 
  //       || nextProps.selected !== this.props.selected;
  // }

  render() {
    console.log(' ItemsList :: render')
    return (
      <div>
        {/* {Date()} */}
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => (
            <div
              onClick={() => this.select(playlist)}
              className={"list-group-item " + (this.props.selected === playlist ? "active" : "")}
              key={playlist.id}
            >
              {index + 1}. {playlist.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
