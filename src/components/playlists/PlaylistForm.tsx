import React from "react";
import { Playlist } from "../../models/Playlist";

type State = {
  playlist: Playlist;
};

type Props = {
  playlist: Playlist;
  onCancel(): void;
  onSave(draft:Playlist): void;
};

export class PlaylistForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    
    // Initialize state:
    this.state = { playlist: this.props.playlist };

    console.log("constructor");
  }

  componentDidMount() {
    console.log("componentDidMount");

    if(this.inputRef.current){
      this.inputRef.current.focus()
    }
  }

  static getDerivedStateFromProps(nextProps: Props, nextState: State) {
    console.log("getDerivedStateFromProps");
    // this.setState({})
    return {
      playlist:
        nextProps.playlist.id === nextState.playlist.id
          ? nextState.playlist
          : nextProps.playlist
    };
  }

  shouldComponentUpdate() {
    console.log("shouldComponentUpdate");
    return true;
  }

  getSnapshotBeforeUpdate() {
    console.log("getSnapshotBeforeUpdate");
    return { placki: 123 };
  }

  componentDidUpdate(nextProps:any,nextState:any,snapshot:any) {
    console.log("componentDidUpdate",snapshot);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const name = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;

    // (this.state.playlist as any)[name] = value

    this.setState({
      playlist: {
        ...this.state.playlist,
        [name]: value
      }
    });
  };

  inputRef = React.createRef<HTMLInputElement>()

  render() {
    console.log('render')
    return (
      <div>
        <div className="form-group">
          <label>Name:</label>
          <input
            type="text"
            className="form-control"
            value={this.state.playlist.name}
            name="name"
            ref={this.inputRef}
            onChange={this.handleChange}
          />
          {255 - this.state.playlist.name.length} / 255
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            checked={this.state.playlist.favorite}
            name="favorite"
            onChange={this.handleChange}
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            value={this.state.playlist.color}
            name="color"
            onChange={this.handleChange}
          />
        </div>

        <input type="button" value="Cancel" onClick={this.props.onCancel} />
        <input type="button" value="Save" onClick={()=>this.props.onSave(this.state.playlist)} />
      </div>
    );
  }
}
