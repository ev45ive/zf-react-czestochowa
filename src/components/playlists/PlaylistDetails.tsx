import React from "react";
import { Playlist } from "../../models/Playlist";

type Props = {
  playlist: Playlist;
  onEdit(): void;
};

export const PlaylistDetails: React.FC<Props> = React.memo(({ playlist, onEdit }) => (
  <div>
    <dl title={playlist.name} data-playlist-id={playlist.id}>
      <dt>Name:</dt>
      <dd>{playlist.name}</dd>

      <dt>Favorite:</dt>
      <dd>{playlist.favorite ? "Yes" : "No"}</dd>

      <dt>Color:</dt>
      <dd
        style={{
          color: playlist.color,
          border: "1px solid " + playlist.color
        }}
      >
        {playlist.color}
      </dd>
    </dl>

    <input type="button" value="Edit" onClick={onEdit} />
  </div>
));
