import React from "react";
// import logo from "./logo.svg";
// import "./App.scss";
// import "bootstrap/scss/bootstrap.scss"

import "bootstrap/dist/css/bootstrap.css";
import { PlaylistsView } from "./views/PlaylistsView";
import { MusicSearchView } from "./views/MusicSearchView";
import { MusicProvider } from "./containers/MusicProvider";
import { Route, Switch, Redirect } from "react-router";
import { Link, NavLink } from "react-router-dom";

const Layout: React.FC = props => (
  <div className="container">
    <div className="row">
      <div className="col">
        {props.children}
      </div>
    </div>
  </div>
);

const App: React.FC = () => {
  return (
    <div className="App">
      <MusicProvider>
        {/* https://getbootstrap.com/docs/4.3/components/navbar/#nav  */}

        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <Link className="navbar-brand" to="/">MusicApp</Link>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink className="nav-link" activeClassName="active placki" to="/music">Search</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <Layout>
          <Switch>
            {/* <Route path="/" exact={true} redirect="/playlists" /> */}
            <Redirect path="/" exact={true} to="/playlists" />
            <Route path="/playlists/:id" component={PlaylistsView} />
            <Route path="/playlists" component={PlaylistsView} />
            <Route path="/music" component={MusicSearchView}  />
            <Route path="*" render={() => <p>404 Page not Found!</p>} />
          </Switch>
        </Layout>
      </MusicProvider>
    </div>
  );
};

export default App;
