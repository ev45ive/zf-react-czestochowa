export class AuthService {
  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type = "token"
  ) {
    this.token = sessionStorage.getItem('token')

    if (!this.token && window.location.hash) {
      const match = window.location.hash.match(/#access_token=([^&]+)/);
      this.token = match && match[1];

      if(this.token){
        window.location.hash = ''
        sessionStorage.setItem('token',this.token)
      }
    }
  }

  token: string | null = null;

  authorize() {
    sessionStorage.removeItem('token')

    const url =
      `${this.auth_url}?client_id=${this.client_id}&` +
      `redirect_uri=${this.redirect_uri}&` +
      `response_type=${this.response_type}`;

    window.location.href = (url);
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
