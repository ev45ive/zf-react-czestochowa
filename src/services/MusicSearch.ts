import axios, { AxiosError } from "axios";
import { auth } from "../services";
import { AlbumsResponse } from '../models/Album';

export class MusicSearch {
  constructor(private api_url: string) {}

  searchAlbums(query = "batman") {
    return axios
      .get<AlbumsResponse>(this.api_url, {
        params: {
          type: "album",
          query
        },
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      })
      .then(resp => resp.data.albums.items)
      .catch((error:AxiosError) => {

        if(error.response!.status === 401){
          auth.authorize()
        }
        
        return Promise.reject(     
          error.response!.data!.error.message 
        )
      })

    }
  }
  


  // .catch(err => {
  //   console.log(err.response.data.error.message); return []
  // })
  ;