import { Reducer, Action, ActionCreator } from "redux";
import { Album } from "../models/Album";

export type MusicState = {
  results: Album[];
  query: string;
  error: string;
};

export const music: Reducer<MusicState, Actions> = (
  state = {
    results: [],
    error: "",
    query: ""
  },
  action
) => {
  switch (action.type) {
    case "MUSIC_SEARCH_START":
      return { ...state, query: action.payload.query, error: "" };
    case "MUSIC_SEARCH_SUCCESS":
      return { ...state, results: action.payload.results };
    case "MUSIC_SEARCH_FAILED":
      return { ...state, error: action.payload.error };
    default:
      return state;
  }
};
type Actions = MUSIC_SEARCH_START | MUSIC_SEARCH_SUCCESS | MUSIC_SEARCH_FAILED;

interface MUSIC_SEARCH_START extends Action<"MUSIC_SEARCH_START"> {
  payload: { query: string };
}
interface MUSIC_SEARCH_SUCCESS extends Action<"MUSIC_SEARCH_SUCCESS"> {
  payload: { results: Album[] };
}
interface MUSIC_SEARCH_FAILED extends Action<"MUSIC_SEARCH_FAILED"> {
  payload: { error: string };
}

export const searchStart: ActionCreator<MUSIC_SEARCH_START> = (
  query: string
) => ({
  type: "MUSIC_SEARCH_START",
  payload: { query }
});

export const searchSuccess: ActionCreator<MUSIC_SEARCH_SUCCESS> = (
  results: Album[]
) => ({
  type: "MUSIC_SEARCH_SUCCESS",
  payload: { results }
});

export const searchFailed: ActionCreator<MUSIC_SEARCH_FAILED> = (
  error: string
) => ({
  type: "MUSIC_SEARCH_FAILED",
  payload: { error }
});
