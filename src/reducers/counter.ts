import { Reducer, Action, ActionCreator } from "redux";

type State = number;

export const counter: Reducer<State,Actions> = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + action.payload;
    case 'DECREMENT':
      return state - action.payload
    default:
      return state;
  }
};
type Actions = INCREMENT | DECREMENT

interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}
interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}

export const inc: ActionCreator<INCREMENT> = (payload = 1) => ({
  type: "INCREMENT",
  payload
});

export const dec: ActionCreator<DECREMENT> = (payload = 1) => ({
  type: "DECREMENT",
  payload
});


(window as any).inc = inc;
(window as any).dec = dec;