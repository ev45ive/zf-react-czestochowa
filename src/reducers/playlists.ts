import { Reducer, Action, ActionCreator } from "redux";
import { Playlist } from "../models/Playlist";

export type PlaylistsState = {
  items: Playlist[];
  selected: Playlist["id"] | null;
};
// https://redux-actions.js.org/introduction/tutorial

export const playlists: Reducer<PlaylistsState, Actions> = (
  state = {
    items: [
      {
        id: 123,
        name: "React Hits",
        favorite: true,
        color: "#ff00ff"
      },
      {
        id: 234,
        name: "React TOP20",
        favorite: false,
        color: "#ffff00"
      },
      {
        id: 345,
        name: "BEst of React",
        favorite: true,
        color: "#00ffff"
      }
    ],
    selected: 123
  },
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_LOAD":
      return { ...state, items: action.payload };
    case "PLAYLIST_ADD":
      return { ...state, items: [...state.items, action.payload] };
    case "PLAYLIST_SELECT":
      return { ...state, selected: action.payload };
    case "PLAYLIST_UPDATE": {
      const draft = action.payload;
      return {
        ...state,
        items: state.items.map(p => (p.id === draft.id ? draft : p))
      };
    }
    case "PLAYLIST_REMOVE":
      return {
        ...state,
        items: state.items.filter(p => p.id !== action.payload)
      };
    default:
      return state;
  }
};

type Actions =
  | PLAYLIST_UPDATE // co to robi?
  | PLAYLISTS_LOAD // laduje placki
  | PLAYLIST_ADD // dodaje placki
  | PLAYLIST_SELECT // zaznacza placki
  | PLAYLIST_REMOVE; // usuwa placki

interface PLAYLISTS_LOAD extends Action<"PLAYLISTS_LOAD"> {
  payload: Playlist[];
}
interface PLAYLIST_UPDATE extends Action<"PLAYLIST_UPDATE"> {
  payload: Playlist;
}
interface PLAYLIST_ADD extends Action<"PLAYLIST_ADD"> {
  payload: Playlist;
}
interface PLAYLIST_SELECT extends Action<"PLAYLIST_SELECT"> {
  payload: Playlist["id"] | null;
}
interface PLAYLIST_REMOVE extends Action<"PLAYLIST_REMOVE"> {
  payload: Playlist["id"] | null;
}

export const playlistsLoad: ActionCreator<PLAYLISTS_LOAD> = (
  payload: Playlist[]
) => ({
  type: "PLAYLISTS_LOAD",
  payload
});

export const playlistUpdate: ActionCreator<PLAYLIST_UPDATE> = (
  payload: Playlist
) => ({
  type: "PLAYLIST_UPDATE",
  payload
});

export const playlistAdd: ActionCreator<PLAYLIST_ADD> = (
  payload: Playlist
) => ({
  type: "PLAYLIST_ADD",
  payload
});

export const playlistSelect: ActionCreator<PLAYLIST_SELECT> = (
  payload: Playlist["id"] | null
) => ({
  type: "PLAYLIST_SELECT",
  payload
});

export const playlistRemove: ActionCreator<PLAYLIST_REMOVE> = (
  payload: Playlist["id"] | null
) => ({
  type: "PLAYLIST_REMOVE",
  payload
});

(window as any).actions = {
  playlistsLoad,
  playlistSelect,
  playlistUpdate,
  playlistRemove
};

/* Selectors */

type AppState = {
  playlists: PlaylistsState;
};

export const selectSelectedPlaylist = (state: AppState) =>
  state.playlists.items.find(p => p.id === state.playlists.selected) || null;
