import { MusicSearch } from "./services/MusicSearch";

// https://wklejaj.pl/AuthService_ts
import { AuthService } from "./services/AuthService";
import React from "react";
import { Album } from "./models/Album";

export const auth = new AuthService(
  "https://accounts.spotify.com/authorize",
  "7a6b379baec0481fb8fca2da5f2f9834",
  "http://localhost:3000/"
);

auth.getToken();

// placki@placki.com
// ******

export const musicSearch = new MusicSearch("https://api.spotify.com/v1/search");

export const musicContext = React.createContext({
  results: [] as Album[],
  query: "",
  search(query:string):void {
    throw "No context provider!";
  }
});
