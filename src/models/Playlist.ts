export interface Playlist {
  id: number | string;
  name: string;
  favorite: boolean;
  /**
   * HEX color
   */
  color: string;
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}
