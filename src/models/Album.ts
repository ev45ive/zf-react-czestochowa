export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists?: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

/* === Server resposes === */

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
