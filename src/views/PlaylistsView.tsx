import React from "react";
import { ItemsList } from "../components/playlists/ItemsList";
import { PlaylistDetails } from "../components/playlists/PlaylistDetails";
import { PlaylistForm } from "../components/playlists/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { store } from "../store";
import { Unsubscribe } from "redux";
import { PlaylistsContainer } from "../containers/PlaylistsContainer";
import {
  SelectedPlaylist,
  SelectedPlaylistForm
} from "../containers/SelectedPlaylist";
import { RouteComponentProps } from "react-router";

type State = {
  mode: "show" | "edit";
  playlists: Playlist[];
  selected: Playlist | null;
};
type P = RouteComponentProps

export class PlaylistsView extends React.Component<P, State> {
  state: State = {
    mode: "show", // 'edit,
    playlists: [],
    selected: null
  };

  edit = () => {
    this.setState({ mode: "edit" });
  };

  cancel = () => {
    this.setState({ mode: "show" });
  };

  componentDidUpdate(props:P){
    console.log(props.match.params)
  }

  render() {
    console.log(this.props)
    return (
      <div>
        <div className="row">
          <div className="col">
            <PlaylistsContainer />
          </div>
          <div className="col">
            {this.state.mode === "show" ? (
              <SelectedPlaylist onEdit={this.edit} />
            ) : null}

            {this.state.mode === "edit" && (
              <SelectedPlaylistForm onCancel={this.cancel} />
            )}

            {/* {!this.state.selected && <p>Please select playlist</p>} */}
          </div>
        </div>
      </div>
    );
  }
}
