import React from "react";
import { MusicSearchForm, MusicSearchResults } from "../containers/MusicSearch";
import { RouteComponentProps } from "react-router";

type P = {} & RouteComponentProps

export class MusicSearchView extends React.Component<P> {
  render() {
    // const params = new URLSearchParams(this.props.location.search)
    // console.log(params.get('q'))

    return (
      <div>
        <div className="row">
          <div className="col">
            <MusicSearchForm />
          </div>
        </div>

        <div className="row">
          <div className="col">
            <MusicSearchResults />
          </div>
        </div>
      </div>
    );
  }
}
