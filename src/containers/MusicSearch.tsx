import { connect } from "react-redux";
import { SearchForm } from "../components/search/SearchForm";
import { SearchResults } from "../components/search/SearchResults";
import {
  MusicState,
  searchStart,
  searchSuccess,
  searchFailed
} from "../reducers/music";
import { musicSearch } from "../services";
import { withRouter, RouteComponentProps } from "react-router";

type AppState = {
  music: MusicState;
};

// https://github.com/supasate/connected-react-router


export const MusicSearchForm = withRouter(
  connect(
    (state: AppState, ownProps: RouteComponentProps) => {
      let query = '' 
      if (ownProps.location.search) {
        query = new URLSearchParams(
          (ownProps as RouteComponentProps).location.search
        ).get("q")!;
      }

      return { query };
    },
    (dispatch) => ({
      onSearch(query: string) {
        dispatch(searchStart(query));

        musicSearch
          .searchAlbums(query)
          .then(results => dispatch(searchSuccess(results)))
          .catch(error => dispatch(searchFailed(error)));
      }
    })
  )(SearchForm)
);

export const MusicSearchResults = connect((state: AppState, ownProps) => {
  return {
    results: state.music.results,
    error: state.music.error
  };
})(SearchResults);

//
