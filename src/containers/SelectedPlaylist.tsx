import { connect } from "react-redux";
import { PlaylistDetails } from "../components/playlists/PlaylistDetails";
import { PlaylistForm } from "../components/playlists/PlaylistForm";
import {
  PlaylistsState,
  selectSelectedPlaylist,
  playlistUpdate
} from "../reducers/playlists";
import { Playlist } from "../models/Playlist";
import { ComponentType } from "react";
import React from "react";

type AppState = {
  playlists: PlaylistsState;
};

export const withPlaylist = connect(
  (state: AppState) => ({
    playlist: selectSelectedPlaylist(state)!
  }),
  dispatch => ({
    onSave: (draft: Playlist) => dispatch(playlistUpdate(draft))
  })
);

export const SelectedPlaylist = withPlaylist(PlaylistDetails);
export const SelectedPlaylistForm = withPlaylist(PlaylistForm);
