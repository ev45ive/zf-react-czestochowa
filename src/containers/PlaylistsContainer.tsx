import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { PlaylistsState, playlistSelect, selectSelectedPlaylist } from '../reducers/playlists';
import { ItemsList } from "../components/playlists/ItemsList";
import { Playlist } from "../models/Playlist";


type TStateProps = {
  playlists: Playlist[];
  selected: Playlist | null;
};
type TOwnProps = {};
type TDispatchProps = {
  onSelect(selected: Playlist): void;
};

type AppState = {
  playlists: PlaylistsState;
};

export const PlaylistsContainer = connect(
  /* Map redux store to component props */
  (state: AppState) => ({
    playlists: state.playlists.items,
    selected: selectSelectedPlaylist(state)
      // state.playlists.items.find(p => p.id === state.playlists.selected) || null
  }),

  /* Map redux actions to component function props */
  dispatch => ({
    onSelect: (playlist: Playlist) => dispatch(playlistSelect(playlist.id))
  })
)(ItemsList);
