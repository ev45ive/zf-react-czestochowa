import React from "react";
import { musicContext, musicSearch } from "../services";
import { Album } from "../models/Album";

type S = {
  results: Album[];
  query: string;
};

export class MusicProvider extends React.Component {
  state: S = {
    results: [
      {
        id: "123",
        name: "Test 123",
        images: [
          {
            url: "http://placekitten.com/200/200"
          }
        ]
      },
      {
        id: "234",
        name: "Test 234",
        images: [
          {
            url: "http://place-hoff.com/400/400"
          }
        ]
      },
      {
        id: "345",
        name: "Test 345",
        images: [
          {
            url: "http://placekitten.com/400/400"
          }
        ]
      },
      {
        id: "456",
        name: "Test 4576",
        images: [
          {
            url: "http://place-hoff.com/300/300"
          }
        ]
      }
    ],
    query: ""
  };

  search = (query: string) => {
    musicSearch.searchAlbums(query).then(results => {
      this.setState({
        results
      });
    });
  };

  render() {
    return (
      <musicContext.Provider
        value={{
          search: this.search,
          ...this.state
        }}
      >
        {this.props.children}
      </musicContext.Provider>
    );
  }
}
