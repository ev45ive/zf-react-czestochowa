import { createStore, Reducer, combineReducers } from "redux";
import { counter } from "./reducers/counter";
import { playlists } from "./reducers/playlists";
import { music } from './reducers/music';

import { connectRouter } from 'connected-react-router'


const reducer = combineReducers({
  counter,
  playlists,
  music,
  router: connectRouter(history),
});

export const store = createStore(
  reducer,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

// debug:
(window as any).store = store;
